
# Description du Pipeline GitLab-CI

Ce pipeline GitLab-CI se compose de plusieurs étapes pour construire, tester, et déployer une application Dockerisée. Voici une description détaillée de chaque étape :

## Stages

### 1. Build
- **docker-build** : Cette étape construit l'image Docker à partir du Dockerfile présent dans le repository et sauvegarde l'image sous forme d'un fichier tar pour les étapes suivantes.
```yaml
docker-build:
  stage: build
  script:
    - docker build -t ${DOCKER_IMAGE} .
    - docker save ${DOCKER_IMAGE} > ${DOCKER_IMAGE}.tar
  artifacts:
    paths:
      - ${DOCKER_IMAGE}.tar
```

### 2. Test acceptation
- **test acceptation** : Cette étape charge l'image Docker précédemment construite et exécute le conteneur pour vérifier qu'une page contenant le terme "Dimension" est accessible.
```yaml
test acception:
  stage: Test acceptation
  script:
    - docker load < ${DOCKER_IMAGE}.tar
    - docker run -d -p ${EXTERNAL_PORT}:${INTERNAL_PORT} ${DOCKER_IMAGE}
    - apk --no-cache add curl
    - curl "http://docker:${EXTERNAL_PORT}" | grep -i "Dimension"
```

### 3. Release image
- **release image** : Cette étape tague l'image Docker et la pousse dans le registre Docker pour les environnements de staging et de production.
```yaml
release image:
  stage: Release image
  script:
    - docker load < ${DOCKER_IMAGE}.tar
    - docker tag ${DOCKER_IMAGE} "${IMAGE_NAME}:${CI_COMMIT_REF_NAME}"
    - docker tag ${DOCKER_IMAGE} "${IMAGE_NAME}:${CI_COMMIT_SHORT_SHA}"
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker push "${IMAGE_NAME}:${CI_COMMIT_REF_NAME}"
    - docker push "${IMAGE_NAME}:${CI_COMMIT_SHORT_SHA}"
```

### 4. Deploy staging
- **deploy staging** : Cette étape déploie l'image Docker sur l'environnement de staging. Elle se connecte au serveur de staging via SSH, supprime l'ancien conteneur et image s'il existe, tire la nouvelle image et exécute le conteneur.
```yaml
deploy staging:
  stage: Deploy staging
  environment:
    name: stage
    url: http://${HOSTNAME_DEPLOY_STAGING}
  script:
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod -R 400 ~/.ssh
    - touch ~/.ssh/known_hosts
    - cd ~/.ssh
    - echo "${SSH_KEY}" > id_rsa
    - chmod 0400 id_rsa
    - ssh-keyscan -t rsa  ${HOSTNAME_DEPLOY_STAGING} >> ~/.ssh/known_hosts
    - command1="docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY"
    - command2="docker rm -f ${DOCKER_IMAGE} || echo 'container does not exist'"
    - command3="docker rmi ${IMAGE_NAME}:${CI_COMMIT_REF_NAME} || echo 'images does not exist'"
    - command4="docker pull ${IMAGE_NAME}:${CI_COMMIT_REF_NAME}"
    - command5="docker run -d -p ${EXTERNAL_PORT}:${INTERNAL_PORT} --name ${DOCKER_IMAGE_STAGING} ${IMAGE_NAME}:${CI_COMMIT_REF_NAME}"
    - ssh -t ${SSH_USER}@${HOSTNAME_DEPLOY_STAGING}
      -o SendEnv=IMAGE_NAME 
      -o SendEnv=CI_COMMIT_REF_NAME 
      -o SendEnv=CI_REGISTRY_USER 
      -o SendEnv=CI_REGISTRY_PASSWORD 
      -o SendEnv=CI_REGISTRY 
      -C "$command1 && $command2 && $command3 && $command4 && $command5"
```

### 5. Deploy release
- **deploy release** : Cette étape déploie l'image Docker sur l'environnement de production. Elle se connecte au serveur de production via SSH, supprime l'ancien conteneur et image s'il existe, tire la nouvelle image et exécute le conteneur.
```yaml
deploy release:
  stage: Deploy release
  environment:
    name: stage
    url: http://${HOSTNAME_DEPLOY_RELEASE}
  script:
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod -R 400 ~/.ssh
    - touch ~/.ssh/known_hosts
    - cd ~/.ssh
    - echo "${SSH_KEY}" > id_rsa
    - chmod 0400 id_rsa
    - ssh-keyscan -t rsa  ${HOSTNAME_DEPLOY_RELEASE} >> ~/.ssh/known_hosts
    - command1="docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY"
    - command2="docker rm -f ${DOCKER_IMAGE_RELEASE} || echo 'container does not exist'"
    - command3="docker rmi ${IMAGE_NAME}:${CI_COMMIT_REF_NAME} || echo 'images does not exist'"
    - command4="docker pull ${IMAGE_NAME}:${CI_COMMIT_REF_NAME}"
    - command5="docker run -d -p ${EXTERNAL_PORT_RELEASE}:${INTERNAL_PORT} --name ${DOCKER_IMAGE_RELEASE} ${IMAGE_NAME}:${CI_COMMIT_REF_NAME}"
    - ssh -t ${SSH_USER}@${HOSTNAME_DEPLOY_RELEASE}
      -o SendEnv=IMAGE_NAME 
      -o SendEnv=CI_COMMIT_REF_NAME 
      -o SendEnv=CI_REGISTRY_USER 
      -o SendEnv=CI_REGISTRY_PASSWORD 
      -o SendEnv=CI_REGISTRY 
      -C "$command1 && $command2 && $command3 && $command4 && $command5"
```

### 6. Validation prod
- **validation prod** : Cette étape valide le déploiement en production en s'assurant que la page contenant le terme "Dimension" est accessible.
```yaml
validation prod:
  <<: *validation
  stage: validation prod
  only:
    - main  
  variables:
    DOMAIN: ${HOSTNAME_DEPLOY_RELEASE}:${EXTERNAL_PORT_RELEASE}
```

### 7. Validation staging
- **validation staging** : Cette étape valide le déploiement en staging en s'assurant que la page contenant le terme "Dimension" est accessible.
```yaml
validation staging:
  <<: *validation
  stage: validation staging
  only:
  - main
  variables:
    DOMAIN: ${HOSTNAME_DEPLOY_STAGING}:${EXTERNAL_PORT}
```

