### STAGE 2: ###
FROM nginx:1.17.1-alpine
ADD . /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
